Haskell Box with Stackage
===========

This is a wercker box containing the latest GHC and a Stackage snapshot instead of Hackage, cabal-install is installed from Stackage, with all exclusive Stackage packages preinstalled

License
-------

MIT

